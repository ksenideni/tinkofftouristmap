package ru.ksenideni.touristmap.showplace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import ru.ksenideni.touristmap.api.GeocodingApi;
import ru.ksenideni.touristmap.exceptions.EntityNotFoundException;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceCreateDTO;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceResponseDTO;
import ru.ksenideni.touristmap.showplace.mapper.ShowplaceCreateMapper;
import ru.ksenideni.touristmap.showplace.mapper.ShowplaceResponseMapper;
import ru.ksenideni.touristmap.showplace.strategy.GreedyRouteStrategy;
import ru.ksenideni.touristmap.api.geocoding.model.Address;
import ru.ksenideni.touristmap.api.geocoding.model.GeocodingResponse;
import ru.ksenideni.touristmap.category.Category;
import ru.ksenideni.touristmap.category.CategoryService;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class ShowplaceServiceTest {
    ShowplaceService serviceUnderTest;
    private ShowplaceRepository repository;
    private ShowplaceCreateMapper showplaceCreateMapper;
    private ShowplaceResponseMapper showplaceResponseMapper;
    private CategoryService categoryService;
    private GreedyRouteStrategy greedyRouteStrategy;
    private GeocodingResponse geocodingResponse;
    private GeocodingApi geocodingApi;

    @BeforeEach
    void setUp() {
        repository = Mockito.mock(ShowplaceRepository.class);
        geocodingApi = Mockito.mock(GeocodingApi.class);
        geocodingResponse = Mockito.mock(GeocodingResponse.class);
        categoryService = Mockito.mock(CategoryService.class);

        showplaceCreateMapper = new ShowplaceCreateMapper();
        showplaceResponseMapper = new ShowplaceResponseMapper();
        greedyRouteStrategy = new GreedyRouteStrategy();

        serviceUnderTest = new ShowplaceService(repository, showplaceCreateMapper,
                showplaceResponseMapper, categoryService, greedyRouteStrategy, geocodingApi);
    }

    @Test
    void findAllSuccess() {
        Category c = new Category(1L, "c1", null, new ArrayList<>());
        Showplace s1 = Showplace.builder()
                .id(1L)
                .name("s1")
                .category(c)
                .longitude(36.5236673)
                .latitude(55.5549675)
                .description("description1")
                .site("www.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(13, 0))
                .price(new BigDecimal(300))
                .build();
        Showplace s2 = Showplace.builder()
                .id(2L)
                .name("s2")
                .category(c)
                .longitude(36.5336673)
                .latitude(55.5349675)
                .description("description2")
                .site("www.com")
                .openTime(LocalTime.of(10, 0))
                .closeTime(LocalTime.of(18, 0))
                .price(new BigDecimal(400))
                .build();
        Mockito.when(repository.findAll()).thenReturn(List.of(s1, s2));
        Mockito.when(geocodingApi.getPlace(ArgumentMatchers.anyDouble(), ArgumentMatchers.anyDouble()))
                .thenReturn(geocodingResponse);
        Mockito.when(geocodingResponse.getAddress()).thenReturn(new Address("Russia", "Moscow"));
        List<ShowplaceResponseDTO> actual = serviceUnderTest.findAll();
        int actualSize = actual.size();
        int expectedSize = 2;
        Assertions.assertEquals(expectedSize, actualSize);
    }

    @Test
    void findByIdSuccess() {
        Long id = 1L;
        Category c = new Category(1L, "c1", null, new ArrayList<>());
        Showplace s1 = Showplace.builder()
                .id(id)
                .name("s1")
                .category(c)
                .longitude(36.5236673)
                .latitude(55.5549675)
                .description("description1")
                .site("www.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(13, 0))
                .price(new BigDecimal(300))
                .build();
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(s1));
        Showplace actualShowplace = serviceUnderTest.findById(id);
        Showplace expectedShowplace = Showplace.builder()
                .id(s1.getId())
                .name(s1.getName())
                .category(s1.getCategory())
                .longitude(s1.getLongitude())
                .latitude(s1.getLatitude())
                .description(s1.getDescription())
                .site(s1.getSite())
                .openTime(s1.getOpenTime())
                .closeTime(s1.getCloseTime())
                .price(s1.getPrice())
                .build();
        Assertions.assertEquals(expectedShowplace, actualShowplace);

    }

    @Test
    void findByIdNotFound() {
        Long id = 1L;
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());
        EntityNotFoundException e = Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> serviceUnderTest.findById(id)
        );
        Assertions.assertEquals("Showplace not found by id = " + id, e.getMessage());
    }

    @Test
    void getByIdSuccess() {
        Long id = 1L;
        Category c = new Category(1L, "c1", null, new ArrayList<>());
        Showplace s1 = Showplace.builder()
                .id(id)
                .name("s1")
                .category(c)
                .longitude(36.5236673)
                .latitude(55.5549675)
                .description("description1")
                .site("www.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(13, 0))
                .price(new BigDecimal(300))
                .build();
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(s1));
        Mockito.when(geocodingApi.getPlace(s1.getLongitude(), s1.getLatitude()))
                .thenReturn(geocodingResponse);
        Mockito.when(geocodingResponse.getAddress())
                .thenReturn(new Address("Russia", "Moscow"));

        ShowplaceResponseDTO expectedShowplace = ShowplaceResponseDTO.builder()
                .id(s1.getId())
                .name(s1.getName())
                .categoryName(c.getName())
                .city("Moscow")
                .description(s1.getDescription())
                .site(s1.getSite())
                .openTime(s1.getOpenTime())
                .closeTime(s1.getCloseTime())
                .price(s1.getPrice())
                .build();
        ShowplaceResponseDTO actualShowplace = serviceUnderTest.getById(id);
        Assertions.assertEquals(expectedShowplace, actualShowplace);
    }

    @Test
    void getByIdNotFound() {
        Long id = 1L;
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());
        EntityNotFoundException e = Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> serviceUnderTest.getById(id)
        );
        Assertions.assertEquals("Showplace not found by id = " + id, e.getMessage());

    }

    @Test
    void updateSuccess() {
        Long id = 1L;
        Category c = new Category(1L, "c1", null, new ArrayList<>());
        Showplace s1 = Showplace.builder()
                .id(id)
                .name("s1")
                .category(c)
                .longitude(36.5236673)
                .latitude(55.5549675)
                .description("description1")
                .site("www.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(13, 0))
                .price(new BigDecimal(300))
                .build();

        ShowplaceCreateDTO updatedShowplace = ShowplaceCreateDTO.builder()
                .id(s1.getId())
                .name("otherName")
                .categoryId(c.getId())
                .latitude(s1.getLatitude())
                .longitude(s1.getLongitude())
                .description(s1.getDescription())
                .site(s1.getSite())
                .openTime(s1.getOpenTime())
                .closeTime(s1.getCloseTime())
                .price(s1.getPrice())
                .build();
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(s1));
        Mockito.when(geocodingApi.getPlace(s1.getLongitude(), s1.getLatitude()))
                .thenReturn(geocodingResponse);
        Mockito.when(geocodingResponse.getAddress())
                .thenReturn(new Address("Russia", "Moscow"));
        Mockito.when(categoryService.findById(updatedShowplace.getCategoryId())).thenReturn(c);

        Showplace showplace = showplaceCreateMapper.fromDTO(updatedShowplace);
        showplace.setCategory(c);
        serviceUnderTest.update(updatedShowplace);
        Mockito.verify(repository, Mockito.times(1)).findById(id);
        Mockito.verify(repository, Mockito.times(1)).save(showplace);

    }

    @Test
    void updateCategoryNotExist() {
        Long id = 1L;
        Category c = new Category(1L, "c1", null, new ArrayList<>());
        Showplace s1 = Showplace.builder()
                .id(id)
                .name("s1")
                .category(c)
                .longitude(36.5236673)
                .latitude(55.5549675)
                .description("description1")
                .site("www.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(13, 0))
                .price(new BigDecimal(300))
                .build();

        ShowplaceCreateDTO updatedShowplace = ShowplaceCreateDTO.builder()
                .id(s1.getId())
                .name("otherName")
                .categoryId(4L)
                .latitude(s1.getLatitude())
                .longitude(s1.getLongitude())
                .description(s1.getDescription())
                .site(s1.getSite())
                .openTime(s1.getOpenTime())
                .closeTime(s1.getCloseTime())
                .price(s1.getPrice())
                .build();
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(s1));
        Mockito.when(geocodingApi.getPlace(s1.getLongitude(), s1.getLatitude()))
                .thenReturn(geocodingResponse);
        Mockito.when(geocodingResponse.getAddress())
                .thenReturn(new Address("Russia", "Moscow"));
        Mockito.when(categoryService.findById(updatedShowplace.getCategoryId()))
                .thenThrow(new EntityNotFoundException("Category not found by id = " + updatedShowplace.getCategoryId()));

        EntityNotFoundException e = Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> serviceUnderTest.update(updatedShowplace)
        );
        Assertions.assertEquals("Category not found by id = " + updatedShowplace.getCategoryId(),
                e.getMessage());

        Mockito.verify(repository, Mockito.times(1)).findById(id);
        Mockito.verify(repository, Mockito.times(0)).save(ArgumentMatchers.any(Showplace.class));
    }

    @Test
    void deleteById() {
        Long id = 1L;
        serviceUnderTest.deleteById(id);
        Mockito.verify(repository, Mockito.times(1)).deleteById(id);
    }

    @Test
    void createSuccess() {
        Category c = new Category(1L, "c1", null, new ArrayList<>());
        ShowplaceCreateDTO newShowplace = ShowplaceCreateDTO.builder()
                .name("otherName")
                .categoryId(1L)
                .longitude(36.5236673)
                .latitude(55.5549675)
                .description("description1")
                .site("www.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(13, 0))
                .price(new BigDecimal(300))
                .build();
        Mockito.when(categoryService.findById(newShowplace.getCategoryId()))
                .thenReturn(c);
        serviceUnderTest.create(newShowplace);
        Mockito.verify(repository, Mockito.times(1))
                .save(ArgumentMatchers.any(Showplace.class));
    }

    @Test
    void createCategoryNotExist() {
        ShowplaceCreateDTO newShowplace = ShowplaceCreateDTO.builder()
                .name("otherName")
                .categoryId(1L)
                .longitude(36.5236673)
                .latitude(55.5549675)
                .description("description1")
                .site("www.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(13, 0))
                .price(new BigDecimal(300))
                .build();
        Mockito.when(categoryService.findById(newShowplace.getCategoryId()))
                .thenThrow(new EntityNotFoundException("Category not found by id = " + newShowplace.getCategoryId()));

        EntityNotFoundException e = Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> serviceUnderTest.create(newShowplace)
        );
        Assertions.assertEquals("Category not found by id = " + newShowplace.getCategoryId(),
                e.getMessage());
        Mockito.verify(repository, Mockito.times(0)).save(ArgumentMatchers.any(Showplace.class));
    }
}