package ru.ksenideni.touristmap.showplace;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.yannbriancon.interceptor.HibernateQueryInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.ksenideni.touristmap.TouristMapApplicationTests;
import ru.ksenideni.touristmap.exceptions.Response;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceCreateDTO;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceResponseDTO;
import ru.ksenideni.touristmap.showplace.dto.UserRequest;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class ShowplaceControllerTest extends TouristMapApplicationTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HibernateQueryInterceptor hibernateQueryInterceptor;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithMockUser(roles = "USER")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES  (1, 'any', null), (2, 'art', 1);
            INSERT INTO showplaces (id, name, description, site, open_time, close_time, price, latitude, longitude, category_id) 
            VALUES (1, 'name', 'description', 'ww.com', '12:00:00', '16:00:00', 200.00, 55.7380244, 37.6046828, 2);
            """)
    void getByIdSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long id = 1L;
        ShowplaceResponseDTO s = ShowplaceResponseDTO.builder()
                .id(id)
                .name("name")
                .description("description")
                .site("ww.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(16, 0))
                .price(new BigDecimal(200).setScale(2))
                .city("Moscow")
                .categoryName("art")
                .build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String expected = mapper.writer().writeValueAsString(s);
        mockMvc.perform(MockMvcRequestBuilders.get("/showplaces/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "USER")
    void getByIdNotFound() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long showplaceId = 1L;
        Response response = new Response("Showplace not found by id = " + showplaceId);
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String expected = mapper.writer().writeValueAsString(response);

        mockMvc.perform(MockMvcRequestBuilders.get("/showplaces/" + showplaceId))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(expected));
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }


    @Test
    @WithMockUser(roles = "ADMIN")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES  (1, 'any', null), (2, 'art', 1);
            INSERT INTO showplaces (id, name, description, site, open_time, close_time, price, latitude, longitude, category_id) 
            VALUES (1, 'name', 'description', 'ww.com', '12:00:00', '16:00:00', 200.00, 55.7380244, 37.6046828, 2);
            """)
    void updateSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long id = 1L;
        ShowplaceCreateDTO updatedDTO = ShowplaceCreateDTO.builder()
                .id(id)
                .name("name_updated")
                .description("description_updated")
                .site("ww.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(16, 0))
                .price(new BigDecimal(200).setScale(2))
                .latitude(55.7380244)
                .longitude(37.6046828)
                .categoryId(2L)
                .build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String updated = mapper.writer().writeValueAsString(updatedDTO);
        mockMvc.perform(MockMvcRequestBuilders.put("/showplaces")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updated))
                .andExpect(status().isOk());
        Assertions.assertEquals(2, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES  (1, 'any', null), (2, 'art', 1);
            """)
    void updateShowplaceNotFound() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long id = 1L;
        ShowplaceCreateDTO updatedDTO = ShowplaceCreateDTO.builder()
                .id(id)
                .name("name_updated")
                .description("description_updated")
                .site("ww.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(16, 0))
                .price(new BigDecimal(200).setScale(2))
                .latitude(55.7380244)
                .longitude(37.6046828)
                .categoryId(2L)
                .build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String updated = mapper.writer().writeValueAsString(updatedDTO);
        Response response = new Response("Can't update showplace because showplace with id=" + id + " not exist");
        String expected = mapper.writer().writeValueAsString(response);
        mockMvc.perform(MockMvcRequestBuilders.put("/showplaces")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updated))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(expected));
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES  (1, 'any', null), (2, 'art', 1);
            INSERT INTO showplaces (id, name, description, site, open_time, close_time, price, latitude, longitude, category_id) 
            VALUES (1, 'name', 'description', 'ww.com', '12:00:00', '16:00:00', 200.00, 55.7380244, 37.6046828, 2);
            """)
    void deleteByIdSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long id = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/showplaces/" + id))
                .andExpect(status().isOk());
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES  (1, 'any', null), (2, 'art', 1);
            """)
    void createSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        ShowplaceCreateDTO createdDTO = ShowplaceCreateDTO.builder()
                .name("name")
                .description("description")
                .site("ww.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(16, 0))
                .price(new BigDecimal(200).setScale(2))
                .latitude(55.7380244)
                .longitude(37.6046828)
                .categoryId(2L)
                .build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String created = mapper.writer().writeValueAsString(createdDTO);
        mockMvc.perform(MockMvcRequestBuilders.post("/showplaces")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(created))
                .andExpect(status().isOk());
        Assertions.assertEquals(2, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createCategoryNotFound() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        ShowplaceCreateDTO createdDTO = ShowplaceCreateDTO.builder()
                .name("name")
                .description("description")
                .site("ww.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(16, 0))
                .price(new BigDecimal(200).setScale(2))
                .latitude(55.7380244)
                .longitude(37.6046828)
                .categoryId(2L)
                .build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String created = mapper.writer().writeValueAsString(createdDTO);
        Response response = new Response("Category not found by id = " + 2L);
        String expected = mapper.writer().writeValueAsString(response);
        mockMvc.perform(MockMvcRequestBuilders.post("/showplaces")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(created))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(expected));
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "USER")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES  (1, 'any', null), (2, 'art', 1);
            INSERT INTO showplaces (id, name, description, site, open_time, close_time, price, latitude, longitude, category_id) 
            VALUES (1, 'name', 'description', 'ww.com', '12:00:00', '16:00:00', 200.00, 55.7380244, 37.6046828, 2),
            (2, 'name2', 'description2', 'ww.com2', '13:00:00', '20:00:00', 100.00, 55.7580244, 37.6246828, 1);
            """)
    void getByCategorySuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long categoryId = 1L;
        ShowplaceResponseDTO dto1 = ShowplaceResponseDTO.builder()
                .id(1L)
                .name("name")
                .description("description")
                .site("ww.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(16, 0))
                .price(new BigDecimal(200).setScale(2))
                .city("Moscow")
                .categoryName("art")
                .build();
        ShowplaceResponseDTO dto2 = ShowplaceResponseDTO.builder()
                .id(2L)
                .name("name2")
                .description("description2")
                .site("ww.com2")
                .openTime(LocalTime.of(13, 0))
                .closeTime(LocalTime.of(20, 0))
                .price(new BigDecimal(100).setScale(2))
                .city("Moscow")
                .categoryName("any")
                .build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String expected = mapper.writer().writeValueAsString(List.of(dto1, dto2));

        mockMvc.perform(MockMvcRequestBuilders.get("/showplaces")
                        .param("category", String.valueOf(categoryId)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
        Assertions.assertEquals(2, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "USER")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES  (1, 'any', null), (2, 'art', 1);
            INSERT INTO showplaces (id, name, description, site, open_time, close_time, price, latitude, longitude, category_id) 
            VALUES (1, 'name', 'description', 'ww.com', '12:00:00', '16:00:00', 200.00, 55.7380244, 37.6046828, 2),
            (2, 'name2', 'description2', 'ww.com2', '13:00:00', '20:00:00', 100.00, 55.7580244, 37.6246828, 1);
            """)
    void getBestRouteEarlyStartAndLateFinish() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        ShowplaceResponseDTO dto1 = ShowplaceResponseDTO.builder()
                .id(1L)
                .name("name")
                .description("description")
                .site("ww.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(16, 0))
                .price(new BigDecimal(200).setScale(2))
                .city("Moscow")
                .categoryName("art")
                .build();
        ShowplaceResponseDTO dto2 = ShowplaceResponseDTO.builder()
                .id(2L)
                .name("name2")
                .description("description2")
                .site("ww.com2")
                .openTime(LocalTime.of(13, 0))
                .closeTime(LocalTime.of(20, 0))
                .price(new BigDecimal(100).setScale(2))
                .city("Moscow")
                .categoryName("any")
                .build();
        List<ShowplaceResponseDTO> expected = List.of(dto1, dto2);
        UserRequest userRequest = UserRequest.builder()
                .city("Moscow")
                .price(new BigDecimal(500).setScale(2))
                .categoriesId(List.of(1L))
                .startTime(LocalTime.of(10, 0))
                .finishTime(LocalTime.of(22, 0))
                .build();

        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.get("/showplaces/route").param("categoriesId", userRequest.getCategoriesId().get(0).toString())
                        .param("startTime", userRequest.getStartTime().toString())
                        .param("finishTime", userRequest.getFinishTime().toString())
                        .param("price", userRequest.getPrice().toString())
                        .param("city", userRequest.getCity()))
                .andExpect(status().isOk())
                .andReturn();
        String actualResult = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        List<ShowplaceResponseDTO> actual = mapper.readValue(actualResult, mapper.getTypeFactory().constructCollectionType(List.class, ShowplaceResponseDTO.class));

        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals(2, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "USER")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES  (1, 'any', null), (2, 'art', 1);
            INSERT INTO showplaces (id, name, description, site, open_time, close_time, price, latitude, longitude, category_id) 
            VALUES (1, 'name', 'description', 'ww.com', '12:00:00', '16:00:00', 200.00, 55.7380244, 37.6046828, 2),
            (2, 'name2', 'description2', 'ww.com2', '13:00:00', '20:00:00', 100.00, 55.7580244, 37.6246828, 1);
            """)
    void getBestRouteStartAndFinishInTheMiddle() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        UserRequest userRequest = UserRequest.builder()
                .city("Moscow")
                .price(new BigDecimal(500).setScale(2))
                .categoriesId(List.of(1L))
                .startTime(LocalTime.of(13, 0))
                .finishTime(LocalTime.of(19, 0))
                .build();

        ShowplaceResponseDTO dto1 = ShowplaceResponseDTO.builder()
                .id(1L)
                .name("name")
                .description("description")
                .site("ww.com")
                .openTime(LocalTime.of(12, 0))
                .closeTime(LocalTime.of(16, 0))
                .price(new BigDecimal(200).setScale(2))
                .city("Moscow")
                .categoryName("art")
                .build();
        ShowplaceResponseDTO dto2 = ShowplaceResponseDTO.builder()
                .id(2L)
                .name("name2")
                .description("description2")
                .site("ww.com2")
                .openTime(LocalTime.of(13, 0))
                .closeTime(LocalTime.of(20, 0))
                .price(new BigDecimal(100).setScale(2))
                .city("Moscow")
                .categoryName("any")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String expected = mapper.writer().writeValueAsString(List.of(dto1, dto2));

        mockMvc.perform(MockMvcRequestBuilders.get("/showplaces/route")
                        .param("categoriesId", userRequest.getCategoriesId().get(0).toString())
                        .param("startTime", userRequest.getStartTime().toString())
                        .param("finishTime", userRequest.getFinishTime().toString())
                        .param("price", userRequest.getPrice().toString())
                        .param("city", userRequest.getCity()))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
        Assertions.assertEquals(2, hibernateQueryInterceptor.getQueryCount());
    }
}