package ru.ksenideni.touristmap.api.geocoding;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ksenideni.touristmap.api.GeocodingApi;
import ru.ksenideni.touristmap.api.geocoding.model.Address;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {GeocodingApi.class}, initializers = ConfigDataApplicationContextInitializer.class)
class GeocodingClientTest {
    @Autowired
    GeocodingApi underTest;

    @Test
    void getPlaceReturnMoscow() {
        Address expected = new Address("Russia", "Moscow");
        Address actual = underTest.getPlace(37.46504, 55.5551)
                .getAddress();
        Assertions.assertEquals(expected, actual);
    }
}