package ru.ksenideni.touristmap.category;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.ksenideni.touristmap.exceptions.EntityNotFoundException;
import ru.ksenideni.touristmap.category.dto.CategoryWithoutParentDTO;
import ru.ksenideni.touristmap.category.mapper.CategoryWithoutParentMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class CategoryServiceTest {
    private CategoryService categoryService;

    private CategoryRepository repository;
    private CategoryWithoutParentMapper mapper;

    @BeforeEach
    void setUp() {
        repository = Mockito.mock(CategoryRepository.class);
        mapper = new CategoryWithoutParentMapper();
        categoryService = new CategoryService(repository, mapper);
    }

    @Test
    void getAllSuccess() {
        Category category1 = Category.builder()
                .id(1L)
                .name("category1")
                .build();
        Category category2 = Category.builder()
                .id(2L)
                .name("category2")
                .parent(category1)
                .build();
        category1.setCategories(List.of(category2));

        Mockito.when(repository.findAll())
                .thenReturn(List.of(category1, category2));
        int expectedSize = 2;
        int actualSize = categoryService.getAll().size();
        Assertions.assertEquals(expectedSize, actualSize);

    }

    @Test
    void findByIdSuccess() {
        Long id = 1L;
        Category category1 = Category.builder()
                .id(id)
                .name("category1")
                .build();
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(category1));
        Category expectedCategory = Category.builder()
                .id(id)
                .name(category1.getName())
                .build();
        Category actualCategory = categoryService.findById(id);
        Assertions.assertEquals(expectedCategory, actualCategory);
    }

    @Test
    void findByIdNotFound() {
        Long id = 1L;
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());
        EntityNotFoundException e = Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> categoryService.findById(id)
        );
        Assertions.assertEquals("Category not found by id = " + id, e.getMessage());
    }

    @Test
    void getByIdSuccess() {
        Long id = 1L;
        Category category1 = Category.builder()
                .id(id)
                .name("category1")
                .build();
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(category1));
        CategoryWithoutParentDTO expectedCategory = CategoryWithoutParentDTO.builder()
                .id(id)
                .name(category1.getName())
                .build();
        CategoryWithoutParentDTO actualCategory = categoryService.getById(id);
        Assertions.assertEquals(expectedCategory, actualCategory);

    }

    @Test
    void getByIdNotFound() {
        Long id = 1L;
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());
        EntityNotFoundException e = Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> categoryService.getById(id)
        );
        Assertions.assertEquals("Category not found by id = " + id, e.getMessage());
    }

    @Test
    void updateSuccess() {
        Long id = 1L;
        Category category1 = Category.builder()
                .id(id)
                .name("category1")
                .build();
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(category1));
        CategoryWithoutParentDTO updatedCategory = CategoryWithoutParentDTO.builder()
                .id(id)
                .name("new_name")
                .build();
        Category category = mapper.fromDTO(updatedCategory);

        categoryService.update(updatedCategory);

        Mockito.verify(repository, Mockito.times(1)).findById(id);
        Mockito.verify(repository, Mockito.times(1)).save(category);
    }

    @Test
    void updateCategoryNotExist() {
        Long id = 2L;
        Category category1 = Category.builder()
                .id(1L)
                .name("category1")
                .build();
        CategoryWithoutParentDTO updatedCategory = CategoryWithoutParentDTO.builder()
                .id(id)
                .name("new_name")
                .build();

        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

        EntityNotFoundException e = Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> categoryService.update(updatedCategory)
        );
        Mockito.verify(repository, Mockito.times(1)).findById(id);
        Assertions.assertEquals("Can't update category because category with id="
                + updatedCategory.getId() + " not exist", e.getMessage());
    }

    @Test
    void deleteById() {
        Long id = 1L;
        categoryService.deleteById(id);
        Mockito.verify(repository, Mockito.times(1)).deleteById(id);
    }

    @Test
    void createSuccessNullParent() {
        CategoryWithoutParentDTO categoryDTO = CategoryWithoutParentDTO.builder()
                .name("name")
                .build();
        Category category = mapper.fromDTO(categoryDTO);
        categoryService.create(null, categoryDTO);
        Mockito.verify(repository, Mockito.times(1)).save(category);

    }

    @Test
    void createSuccessNotNullParent() {
        Long parentId = 1L;
        CategoryWithoutParentDTO categoryDTO = CategoryWithoutParentDTO.builder()
                .name("name")
                .build();
        Category parentCategory = Category.builder()
                .id(parentId)
                .name("parent_name")
                .build();

        Mockito.when(repository.findById(parentId)).thenReturn(Optional.of(parentCategory));
        Category category = mapper.fromDTO(categoryDTO);
        category.setParent(parentCategory);
        categoryService.create(parentId, categoryDTO);
        Mockito.verify(repository, Mockito.times(1)).save(category);
    }

    @Test
    void createParentNotFound() {
        Long parentId = 1L;
        CategoryWithoutParentDTO categoryDTO = CategoryWithoutParentDTO.builder()
                .name("name")
                .build();
        Category parentCategory = Category.builder()
                .id(parentId)
                .name("parent_name")
                .build();

        Mockito.when(repository.findById(parentId)).thenReturn(Optional.empty());
        Category category = mapper.fromDTO(categoryDTO);
        category.setParent(parentCategory);

        EntityNotFoundException e = Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> categoryService.create(parentId, categoryDTO)
        );
        Mockito.verify(repository, Mockito.times(0)).save(category);
        Assertions.assertEquals("Category not found by id = " + parentId, e.getMessage());
    }

    @Test
    void getAllSubcategoriesSimple() {
        Long parentId = 1L;
        Category category1 = Category.builder()
                .id(parentId)
                .name("category1")
                .build();
        Category category2 = Category.builder()
                .id(2L)
                .name("category2")
                .parent(category1)
                .categories(new ArrayList<>())
                .build();
        category1.setCategories(List.of(category2));

        Mockito.when(repository.findSubcatecories(parentId))
                .thenReturn(List.of(category1, category2));
        int expectedSize = 2;
        int actualSize = categoryService.getAllSubcategories(parentId).size();
        Assertions.assertEquals(expectedSize, actualSize);
    }

    @Test
    void getAllSubcategoriesNestedRequest() {
        /*
        вложенность категорий (берутся все дочерние и родитель, т.е. 5)
        0
            1
            2
                3
                4
         */

        Category category0 = Category.builder()
                .id(1L)
                .name("category0")
                .build();

        Category category1 = Category.builder()
                .id(2L)
                .name("category1")
                .parent(category0)
                .categories(new ArrayList<>())
                .build();
        Category category2 = Category.builder()
                .id(3L)
                .name("category2")
                .parent(category0)
                .categories(new ArrayList<>())
                .build();
        category0.setCategories(List.of(category1, category2));

        Category category3 = Category.builder()
                .id(4L)
                .name("category3")
                .parent(category2)
                .categories(new ArrayList<>())
                .build();
        Category category4 = Category.builder()
                .id(5L)
                .name("category4")
                .parent(category2)
                .categories(new ArrayList<>())
                .build();
        category2.setCategories(List.of(category3, category4));

        Mockito.when(repository.findSubcatecories(category0.getId()))
                .thenReturn(List.of(category0, category1, category2, category3, category4));
        List<Category> actualCategories = categoryService.getAllSubcategories(category0.getId());

        int expectedNumberOfSubcategories = 5;
        int actualNumberOfSubcategories = actualCategories.size();
        Assertions.assertEquals(expectedNumberOfSubcategories, actualNumberOfSubcategories);
    }
}