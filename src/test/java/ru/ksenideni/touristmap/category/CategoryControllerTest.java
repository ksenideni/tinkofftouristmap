package ru.ksenideni.touristmap.category;

import com.yannbriancon.interceptor.HibernateQueryInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.ksenideni.touristmap.TouristMapApplicationTests;
import ru.ksenideni.touristmap.exceptions.Response;
import ru.ksenideni.touristmap.category.dto.CategoryWithoutParentDTO;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class CategoryControllerTest extends TouristMapApplicationTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HibernateQueryInterceptor hibernateQueryInterceptor;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithMockUser(roles = "USER")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES  (1, 'any', null), (2, 'science', 1)
            """)
    void findAllSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        CategoryWithoutParentDTO c1 = CategoryWithoutParentDTO.builder()
                .id(1L)
                .name("any")
                .build();
        CategoryWithoutParentDTO c2 = CategoryWithoutParentDTO.builder()
                .id(2L)
                .name("science")
                .build();
        String expected = new ObjectMapper().writer().writeValueAsString(List.of(c1, c2));
        mockMvc.perform(MockMvcRequestBuilders.get("/categories"))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "USER")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES (1, 'category_name', null)
            """)
    void getByIdSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long id = 1L;
        CategoryWithoutParentDTO c1 = CategoryWithoutParentDTO.builder()
                .id(id)
                .name("category_name")
                .build();
        String expected = new ObjectMapper().writer().writeValueAsString(c1);
        mockMvc.perform(MockMvcRequestBuilders.get("/categories/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "USER")
    void getByIdNotFound() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long id = 1L;
        Response response = new Response("Category not found by id = " + id);
        String expected = new ObjectMapper().writer().writeValueAsString(response);
        mockMvc.perform(MockMvcRequestBuilders.get("/categories/" + id))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(expected));
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES (1, 'любое', null)
            """)
    void updateSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long id = 1L;
        CategoryWithoutParentDTO c1 = CategoryWithoutParentDTO.builder()
                .id(id)
                .name("любое_изменено")
                .build();
        String updated = new ObjectMapper().writer().writeValueAsString(c1);
        mockMvc.perform(MockMvcRequestBuilders.put("/categories")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updated))
                .andExpect(status().isOk());
        Assertions.assertEquals(2, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateNotFound() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long id = 1L;
        CategoryWithoutParentDTO c1 = CategoryWithoutParentDTO.builder()
                .id(id)
                .name("любое_изменено")
                .build();
        String updated = new ObjectMapper().writer().writeValueAsString(c1);
        Response response = new Response("Can't update category because category with id="
                + id + " not exist");
        String expected = new ObjectMapper().writer().writeValueAsString(response);

        mockMvc.perform(MockMvcRequestBuilders.put("/categories")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updated))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(expected));
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @Sql(statements = """
            INSERT INTO categories (name, parent_id) VALUES ('любое', null)
            """)
    void createNonRootSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long parentId = 1L;
        CategoryWithoutParentDTO c1 = CategoryWithoutParentDTO.builder()
                .name("наука")
                .build();
        String created = new ObjectMapper().writer().writeValueAsString(c1);
        mockMvc.perform(MockMvcRequestBuilders.post("/categories/" + parentId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(created))
                .andExpect(status().isOk());
        Assertions.assertEquals(2, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void creatNonRootParentNotFound() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long parentId = 1L;
        CategoryWithoutParentDTO c1 = CategoryWithoutParentDTO.builder()
                .name("наука")
                .build();
        String created = new ObjectMapper().writer().writeValueAsString(c1);
        String expected = new ObjectMapper().writer()
                .writeValueAsString(new Response("Category not found by id = " + parentId));
        mockMvc.perform(MockMvcRequestBuilders.post("/categories/" + parentId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(created))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(expected));
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createSuccessRootCategory() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        CategoryWithoutParentDTO c1 = CategoryWithoutParentDTO.builder()
                .name("наука")
                .build();
        String created = new ObjectMapper().writer().writeValueAsString(c1);

        mockMvc.perform(MockMvcRequestBuilders.post("/categories/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(created))
                .andExpect(status().isOk());
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES (1, 'любое', null)
            """)
    void deleteById() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long id = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/categories/" + id))
                .andExpect(status().isOk());
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    @WithMockUser(roles = "USER")
    @Sql(statements = """
            INSERT INTO categories (id, name, parent_id) VALUES (1, 'category_name1', null), (2, 'category_name2', 1)
            """)
    void getSubcategoriesSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();
        Long parentId = 1L;
        String expected = new ObjectMapper().writer().writeValueAsString(List.of("category_name1", "category_name2"));

        mockMvc.perform(MockMvcRequestBuilders.get("/categories/" + parentId + "/subcategories"))
                .andExpect(content().string(expected))
                .andExpect(status().isOk());
        Assertions.assertEquals(1, hibernateQueryInterceptor.getQueryCount());
    }
}