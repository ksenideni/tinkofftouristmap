CREATE TABLE categories
(
    id        BIGSERIAL PRIMARY KEY,
    name      VARCHAR(255),
    parent_id BIGINT REFERENCES categories (id) ON DELETE CASCADE
);

CREATE TABLE showplaces
(
    id          BIGSERIAL PRIMARY KEY,
    name        VARCHAR(255),
    close_time  TIME,
    description TEXT,
    latitude    DOUBLE PRECISION NOT NULL
        CONSTRAINT showplaces_latitude_check
            check ((latitude <= (180)::double precision) AND (latitude >= ('-180'::integer)::double precision)),
    longitude   double precision not null
        constraint showplaces_longitude_check
            check ((longitude <= (90)::double precision) AND (longitude >= ('-90'::integer)::double precision)),
    open_time   TIME,
    price       NUMERIC(19, 2),
    site        VARCHAR(255),
    category_id BIGINT REFERENCES categories (id)
);

CREATE TABLE users
(
    username VARCHAR(50)  NOT NULL,
    password VARCHAR(100) NOT NULL,
    enabled  BOOLEAN      NOT NULL DEFAULT TRUE,
    PRIMARY KEY (username)
);

CREATE TABLE authorities
(
    username  VARCHAR(50) NOT NULL,
    authority VARCHAR(50) NOT NULL,
    FOREIGN KEY (username) REFERENCES users (username)
);

INSERT INTO users (username, password, enabled)
VALUES ('admin', '$2a$12$Se3O5zNDIuMMCrwcidwQzeJFMvArMijuf9OeKnnuGzDiWINuJ1eWC', true);

INSERT INTO authorities (username, authority)
VALUES ('admin', 'ROLE_ADMIN'),
       ('admin', 'ROLE_USER');