package ru.ksenideni.touristmap.showplace;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.ksenideni.touristmap.category.Category;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "showplaces")
public class Showplace {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "longitude", nullable = false)
    private Double longitude;

    @Column(name = "latitude", nullable = false)
    private Double latitude;

    @Column(name = "description")
    private String description;

    @Column(name = "site")
    private String site;

    @Column(name = "open_time")
    private LocalTime openTime;

    @Column(name = "close_time")
    private LocalTime closeTime;

    @Column(name = "price")
    private BigDecimal price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Showplace showplace = (Showplace) o;
        return Objects.equals(id, showplace.id) && Objects.equals(name, showplace.name) && Objects.equals(category, showplace.category) && Objects.equals(longitude, showplace.longitude) && Objects.equals(latitude, showplace.latitude) && Objects.equals(description, showplace.description) && Objects.equals(site, showplace.site) && Objects.equals(openTime, showplace.openTime) && Objects.equals(closeTime, showplace.closeTime) && Objects.equals(price, showplace.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, category, longitude, latitude, description, site, openTime, closeTime, price);
    }
}
