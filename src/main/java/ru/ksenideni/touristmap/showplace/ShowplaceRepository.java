package ru.ksenideni.touristmap.showplace;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.ksenideni.touristmap.category.Category;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface ShowplaceRepository extends JpaRepository<Showplace, Long> {
    List<Showplace> findAllByCategoryIn(List<Category> categories);

    List<Showplace> findAllByCategoryInAndOpenTimeLessThanEqualAndCloseTimeGreaterThanEqualAndPriceLessThanEqual(
            List<Category> categories, LocalTime finishTime, LocalTime startTime, BigDecimal price, Sort sort
    );

    @Modifying
    @Query(value = "delete from showplaces where id=?1", nativeQuery = true)
    void deleteById(Long id);
}
