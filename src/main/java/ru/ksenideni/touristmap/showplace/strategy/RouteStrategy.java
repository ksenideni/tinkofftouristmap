package ru.ksenideni.touristmap.showplace.strategy;

import ru.ksenideni.touristmap.showplace.dto.ShowplaceResponseDTO;
import ru.ksenideni.touristmap.showplace.dto.UserRequest;

import java.util.List;

@FunctionalInterface
public interface RouteStrategy {
    List<ShowplaceResponseDTO> getRoute(List<ShowplaceResponseDTO> showplaces, UserRequest userRequest);
}
