package ru.ksenideni.touristmap.showplace.strategy;

import org.springframework.stereotype.Component;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceResponseDTO;
import ru.ksenideni.touristmap.showplace.dto.UserRequest;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class GreedyRouteStrategy implements RouteStrategy {
    @Override
    public List<ShowplaceResponseDTO> getRoute(List<ShowplaceResponseDTO> showplaces, UserRequest userRequest) {
        showplaces = showplaces.stream().filter(showplaceResponseDTO -> showplaceResponseDTO.getCity().equals(userRequest.getCity())).toList();
        LocalTime iterator;
        if (showplaces.get(0).getOpenTime().compareTo(userRequest.getStartTime()) > 0) {
            iterator = showplaces.get(0).getOpenTime();
        } else {
            iterator = userRequest.getStartTime();
        }
        List<ShowplaceResponseDTO> route = new ArrayList<>();
        BigDecimal remaining = userRequest.getPrice();
        for (int i = 0; i < showplaces.size(); i++) {
            ShowplaceResponseDTO currentShowplace = showplaces.get(i);
            LocalTime currentOpenTime = currentShowplace.getOpenTime();
            LocalTime currentCloseTime = currentShowplace.getCloseTime();

            //захожу позже чем закрывается - скип
            if (iterator.compareTo(currentCloseTime) >= 0) continue;

            //мое время меньше открытия - нужно ждать до открытия
            if (iterator.compareTo(currentOpenTime) < 0) {
                iterator = currentOpenTime;
            }

            //чекнуть если прогулка закончена - завершить итерацию
            if (iterator.compareTo(userRequest.getFinishTime()) >= 0) break;

            //пробыть час - до закрытия
            if (iterator.plusHours(1).compareTo(currentCloseTime) <= 0 && remaining.compareTo(currentShowplace.getPrice()) >= 0) {
                remaining = remaining.subtract(currentShowplace.getPrice());
                iterator = iterator.plusHours(1);
                route.add(showplaces.get(i));
            }
        }
        return route;
    }
}
