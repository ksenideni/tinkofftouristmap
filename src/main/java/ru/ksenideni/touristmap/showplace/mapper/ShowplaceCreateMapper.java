package ru.ksenideni.touristmap.showplace.mapper;

import org.springframework.stereotype.Component;
import ru.ksenideni.touristmap.showplace.Showplace;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceCreateDTO;

@Component
public class ShowplaceCreateMapper {
    public Showplace fromDTO(ShowplaceCreateDTO showplaceCreateDTO) {
        Showplace buildShowplace = Showplace.builder()
                .id(showplaceCreateDTO.getId())
                .name(showplaceCreateDTO.getName())
                .longitude(showplaceCreateDTO.getLongitude())
                .latitude(showplaceCreateDTO.getLatitude())
                .description(showplaceCreateDTO.getDescription())
                .site(showplaceCreateDTO.getSite())
                .openTime(showplaceCreateDTO.getOpenTime())
                .closeTime(showplaceCreateDTO.getCloseTime())
                .price(showplaceCreateDTO.getPrice())
                .build();
        return buildShowplace;
    }
}
