package ru.ksenideni.touristmap.showplace.mapper;

import org.springframework.stereotype.Component;
import ru.ksenideni.touristmap.showplace.Showplace;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceResponseDTO;

@Component
public class ShowplaceResponseMapper {
    public ShowplaceResponseDTO fromEntity(Showplace showplace, String city) {
        return ShowplaceResponseDTO.builder()
                .id(showplace.getId())
                .name(showplace.getName())
                .categoryName(showplace.getCategory().getName())
                .city(city)
                .description(showplace.getDescription())
                .site(showplace.getSite())
                .openTime(showplace.getOpenTime())
                .closeTime(showplace.getCloseTime())
                .price(showplace.getPrice())
                .build();
    }
}
