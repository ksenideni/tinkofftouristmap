package ru.ksenideni.touristmap.showplace.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShowplaceCreateDTO {
    private Long id;

    @NotBlank(message = "The showplace name cannot be empty")
    private String name;

    @Min(value = 1, message = "The minimum category id is 1")
    private Long categoryId;

    @DecimalMax(value = "90.0", message = "The maximum longitude value is 90")
    @DecimalMin(value = "-90.0", message = "The minimum longitude value is -90")
    private Double longitude;

    @DecimalMax(value = "180.0", message = "The maximum longitude value is 180")
    @DecimalMin(value = "-180.0", message = "The minimum longitude value is -180")
    private Double latitude;

    private String description;

    private String site;

    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime openTime;

    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime closeTime;

    private BigDecimal price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShowplaceCreateDTO that = (ShowplaceCreateDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(categoryId, that.categoryId) && Objects.equals(longitude, that.longitude) && Objects.equals(latitude, that.latitude) && Objects.equals(description, that.description) && Objects.equals(site, that.site) && Objects.equals(openTime, that.openTime) && Objects.equals(closeTime, that.closeTime) && Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, categoryId, longitude, latitude, description, site, openTime, closeTime, price);
    }
}
