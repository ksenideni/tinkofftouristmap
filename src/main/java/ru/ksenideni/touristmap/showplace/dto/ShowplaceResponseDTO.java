package ru.ksenideni.touristmap.showplace.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShowplaceResponseDTO {
    private Long id;

    private String name;

    private String categoryName;

    private String city;

    private String description;

    private String site;

    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime openTime;

    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime closeTime;

    private BigDecimal price;

    @Override
    public String toString() {
        return "ShowplaceResponseDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", city='" + city + '\'' +
                ", description='" + description + '\'' +
                ", site='" + site + '\'' +
                ", openTime=" + openTime +
                ", closeTime=" + closeTime +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShowplaceResponseDTO that = (ShowplaceResponseDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(categoryName, that.categoryName) && Objects.equals(city, that.city) && Objects.equals(description, that.description) && Objects.equals(site, that.site) && Objects.equals(openTime, that.openTime) && Objects.equals(closeTime, that.closeTime) && Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, categoryName, city, description, site, openTime, closeTime, price);
    }
}
