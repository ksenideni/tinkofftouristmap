package ru.ksenideni.touristmap.showplace;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ksenideni.touristmap.showplace.dto.UserRequest;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceCreateDTO;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceResponseDTO;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/showplaces")
public class ShowplaceController {
    private final ShowplaceService showplaceService;

    @GetMapping
    public List<ShowplaceResponseDTO> findAll() {
        return showplaceService.findAll();
    }

    @GetMapping("/{id}")
    public ShowplaceResponseDTO getById(@PathVariable("id") Long id) {
        return showplaceService.getById(id);
    }

    @PutMapping
    public void update(@RequestBody @Valid ShowplaceCreateDTO showplaceCreateDTO) {
        showplaceService.update(showplaceCreateDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        showplaceService.deleteById(id);
    }

    @PostMapping
    public void create(@RequestBody @Valid ShowplaceCreateDTO showplaceCreateDTO) {
        showplaceService.create(showplaceCreateDTO);
    }

    @GetMapping(params = "category")
    public List<ShowplaceResponseDTO> getByCategory(@RequestParam("category") Long categoryId) {
        return showplaceService.getByCategory(categoryId);
    }

    @GetMapping("/route")
    public List<ShowplaceResponseDTO> getRoute(UserRequest userRequest) {
        return showplaceService.getRoute(userRequest);
    }
}
