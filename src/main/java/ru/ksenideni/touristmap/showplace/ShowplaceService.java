package ru.ksenideni.touristmap.showplace;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.ksenideni.touristmap.api.GeocodingApi;
import ru.ksenideni.touristmap.category.Category;
import ru.ksenideni.touristmap.category.CategoryService;
import ru.ksenideni.touristmap.exceptions.EntityNotFoundException;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceCreateDTO;
import ru.ksenideni.touristmap.showplace.dto.ShowplaceResponseDTO;
import ru.ksenideni.touristmap.showplace.dto.UserRequest;
import ru.ksenideni.touristmap.showplace.mapper.ShowplaceCreateMapper;
import ru.ksenideni.touristmap.showplace.mapper.ShowplaceResponseMapper;
import ru.ksenideni.touristmap.showplace.strategy.GreedyRouteStrategy;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ShowplaceService {
    private final ShowplaceRepository showplaceRepository;
    private final ShowplaceCreateMapper showplaceCreateMapper;
    private final ShowplaceResponseMapper showplaceResponseMapper;
    private final CategoryService categoryService;
    private final GreedyRouteStrategy greedyRouteStrategy;
    private final GeocodingApi geocodingApi;

    public List<ShowplaceResponseDTO> findAll() {
        return showplaceRepository.findAll().stream().map(showplace -> {
            String city = geocodingApi.getPlace(
                    showplace.getLongitude(), showplace.getLatitude()
            ).getAddress().getCity();
            return showplaceResponseMapper.fromEntity(showplace, city);
        }).toList();
    }

    public Showplace findById(Long id) {
        return showplaceRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Showplace not found by id = " + id));
    }

    public ShowplaceResponseDTO getById(Long id) {
        Showplace showplace = findById(id);
        String city = geocodingApi
                .getPlace(showplace.getLongitude(), showplace.getLatitude())
                .getAddress()
                .getCity();

        return showplaceResponseMapper.fromEntity(showplace, city);
    }


    @Transactional
    public void update(ShowplaceCreateDTO updatedShowplaceCreateDTO) {
        Optional<Showplace> showplace = showplaceRepository.findById(updatedShowplaceCreateDTO.getId());
        if (showplace.isPresent()) {
            Category category = categoryService.findById(updatedShowplaceCreateDTO.getCategoryId());
            Showplace updatedShowplace = showplaceCreateMapper.fromDTO(updatedShowplaceCreateDTO);
            updatedShowplace.setCategory(category);
            showplaceRepository.save(updatedShowplace);
        } else {
            throw new EntityNotFoundException("Can't update showplace because showplace with id=" +
                    updatedShowplaceCreateDTO.getId() + " not exist");
        }
    }

    public void deleteById(Long id) {
        showplaceRepository.deleteById(id);
    }

    @Transactional
    public void create(ShowplaceCreateDTO showplaceCreateDTO) {
        Category category = categoryService.findById(showplaceCreateDTO.getCategoryId());
        Showplace createdShowplace = showplaceCreateMapper.fromDTO(showplaceCreateDTO);
        createdShowplace.setCategory(category);
        showplaceRepository.save(createdShowplace);
    }

    @Transactional
    public List<ShowplaceResponseDTO> getByCategory(Long categoryId) {
        List<Category> categories = categoryService.getAllSubcategories(categoryId);
        return showplaceRepository.findAllByCategoryIn(categories)
                .stream()
                .map(showplace -> {
                    String city = geocodingApi.getPlace(
                            showplace.getLongitude(), showplace.getLatitude()
                    ).getAddress().getCity();
                    return showplaceResponseMapper.fromEntity(showplace, city);
                })
                .collect(Collectors.toList());
    }


    public List<ShowplaceResponseDTO> getRoute(UserRequest userRequest) {
        List<Category> categories = new ArrayList<>();
        for (Long c : userRequest.getCategoriesId()) {
            categories.addAll(categoryService.getAllSubcategories(c));
        }
        List<ShowplaceResponseDTO> showplaces = showplaceRepository
                .findAllByCategoryInAndOpenTimeLessThanEqualAndCloseTimeGreaterThanEqualAndPriceLessThanEqual(
                        categories,
                        userRequest.getFinishTime(),
                        userRequest.getStartTime(),
                        userRequest.getPrice(),
                        Sort.by("openTime", "closeTime", "price"))
                .stream()
                .map(showplace -> {
                    String city = geocodingApi
                            .getPlace(showplace.getLongitude(), showplace.getLatitude())
                            .getAddress()
                            .getCity();
                    return showplaceResponseMapper.fromEntity(showplace, city);
                })
                .toList();
        if(showplaces.isEmpty()){
            throw new EntityNotFoundException("Not found Showplaces");
        }
        return greedyRouteStrategy.getRoute(showplaces, userRequest);
    }

    public List<ShowplaceResponseDTO> findAllByCategoryIn(List<Category> categories) {
        return showplaceRepository.findAllByCategoryIn(categories).stream().map(showplace -> {
                    String city = geocodingApi
                            .getPlace(showplace.getLongitude(), showplace.getLatitude())
                            .getAddress()
                            .getCity();
                    return showplaceResponseMapper.fromEntity(showplace, city);
                })
                .toList();
    }
}
