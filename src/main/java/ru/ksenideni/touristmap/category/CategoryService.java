package ru.ksenideni.touristmap.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.ksenideni.touristmap.exceptions.EntityNotFoundException;
import ru.ksenideni.touristmap.category.dto.CategoryWithoutParentDTO;
import ru.ksenideni.touristmap.category.mapper.CategoryWithoutParentMapper;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryWithoutParentMapper categoryWithoutParentMapper;

    public List<CategoryWithoutParentDTO> getAll() {
        return categoryRepository.findAll().stream().map(categoryWithoutParentMapper::fromEntity).toList();
    }

    public Category findById(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        if (category.isPresent()) {
            return category.get();
        } else {
            throw new EntityNotFoundException("Category not found by id = " + id);
        }
    }

    public CategoryWithoutParentDTO getById(Long id) {
        return categoryWithoutParentMapper.fromEntity(findById(id));
    }

    @Transactional
    public void update(CategoryWithoutParentDTO updatedCategoryDTO) {
        Optional<Category> category = categoryRepository.findById(updatedCategoryDTO.getId());
        if (category.isPresent()) {
            Category parentCategory = category.get().getParent();
            Category updated = categoryWithoutParentMapper.fromDTO(updatedCategoryDTO);
            updated.setParent(parentCategory);
            categoryRepository.save(updated);
        } else {
            throw new EntityNotFoundException("Can't update category because category with id="
                    + updatedCategoryDTO.getId() + " not exist");
        }
    }

    public void deleteById(Long id) {
        categoryRepository.deleteById(id);
    }

    @Transactional
    public void create(Long parentId, CategoryWithoutParentDTO categoryDTO) {
        //создание корневой категории
        if (parentId == null) {
            categoryRepository.save(categoryWithoutParentMapper.fromDTO(categoryDTO));
        } else {
            Category parentCategory = findById(parentId);
            Category created = categoryWithoutParentMapper.fromDTO(categoryDTO);
            created.setParent(parentCategory);
            categoryRepository.save(created);
        }
    }

    @Transactional
    public List<Category> getAllSubcategories(Long parentId) {
        return categoryRepository.findSubcatecories(parentId);
    }

    public List<String> getNameOfSubcategories(Long parentId) {
        List<Category> result = getAllSubcategories(parentId);
        return result.stream().map(Category::getName).toList();
    }
}
