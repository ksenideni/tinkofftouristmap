package ru.ksenideni.touristmap.category;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ksenideni.touristmap.category.dto.CategoryWithoutParentDTO;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/categories")
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping
    public List<CategoryWithoutParentDTO> findAll() {
        return categoryService.getAll();
    }

    @GetMapping("/{id}")
    public CategoryWithoutParentDTO getById(@PathVariable("id") Long id) {
        return categoryService.getById(id);
    }

    @PutMapping
    public void update(@RequestBody @Valid CategoryWithoutParentDTO category) {
        categoryService.update(category);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        categoryService.deleteById(id);
    }

    @PostMapping("/{parentid}")
    public void create(@PathVariable("parentid") Long parentId, @RequestBody @Valid CategoryWithoutParentDTO categoryDTO) {
        categoryService.create(parentId, categoryDTO);
    }

    @PostMapping
    public void create(@RequestBody @Valid CategoryWithoutParentDTO categoryDTO) {
        categoryService.create(null, categoryDTO);
    }

    @GetMapping("/{parentid}/subcategories")
    public List<String> getSubcategories(@PathVariable("parentid") Long parentId) {
        return categoryService.getNameOfSubcategories(parentId);
    }
}
