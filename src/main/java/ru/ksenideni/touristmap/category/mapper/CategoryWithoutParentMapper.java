package ru.ksenideni.touristmap.category.mapper;

import org.springframework.stereotype.Component;
import ru.ksenideni.touristmap.category.Category;
import ru.ksenideni.touristmap.category.dto.CategoryWithoutParentDTO;

@Component
public class CategoryWithoutParentMapper {
    public CategoryWithoutParentDTO fromEntity(Category category) {
        CategoryWithoutParentDTO buildCategoryDTO = CategoryWithoutParentDTO.builder()
                .id(category.getId())
                .name(category.getName())
                .build();
        return buildCategoryDTO;
    }

    public Category fromDTO(CategoryWithoutParentDTO categoryDTO) {
        Category buildCategory = Category.builder()
                .id(categoryDTO.getId())
                .name(categoryDTO.getName())
                .build();
        return buildCategory;
    }
}
