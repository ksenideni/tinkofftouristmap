package ru.ksenideni.touristmap.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query(value = """
            WITH RECURSIVE empdata AS (
              (SELECT id, name, parent_id, 1 AS level
              FROM categories
              WHERE id = ?1)
              UNION ALL
              (SELECT this.id, this.name, this.parent_id, prior.level + 1
              FROM empdata prior
              INNER JOIN categories this ON this.parent_id = prior.id)
            )
            SELECT e.id, e.name, e.parent_id
            FROM empdata e
            ORDER BY e.level""", nativeQuery = true)
    List<Category> findSubcatecories(Long parentId);

    @Modifying
    @Query(value = "delete from categories where id=?1", nativeQuery = true)
    void deleteById(Long id);
}
