package ru.ksenideni.touristmap.security;

public enum UserRole {
    USER,
    ADMIN
}
