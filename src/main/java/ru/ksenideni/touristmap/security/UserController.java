package ru.ksenideni.touristmap.security;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor

public class UserController {
    private final UserService userService;

    @PostMapping("/registration")
    public void registerUser(@Valid @RequestBody UserDTO user) {
        userService.registerNewUserAccount(user);
    }
}
