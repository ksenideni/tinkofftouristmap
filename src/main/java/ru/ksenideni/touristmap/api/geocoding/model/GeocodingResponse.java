package ru.ksenideni.touristmap.api.geocoding.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeocodingResponse {

    @JsonProperty("query")
    List<Double> query;

    @JsonProperty("features")
    private List<FeaturesResponse> featuresResponse;

    public Address getAddress() {
        if (featuresResponse.isEmpty()) {
            throw new IllegalStateException("Invalid geographical coordinates: " + query);
        }
        return Address.builder()
                .country(featuresResponse.get(featuresResponse.size() - 1).getPlaceName())
                .city(featuresResponse.size() > 2 ? featuresResponse.get(featuresResponse.size() - 2).getPlaceName() : null)
                .build();
    }
}
