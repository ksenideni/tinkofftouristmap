package ru.ksenideni.touristmap.api.geocoding.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
class FeaturesResponse {
    @JsonProperty("text")
    private String placeName;

    public String getPlaceName() {
        return placeName;
    }
}
