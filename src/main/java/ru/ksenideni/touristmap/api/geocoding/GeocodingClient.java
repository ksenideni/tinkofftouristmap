package ru.ksenideni.touristmap.api.geocoding;

import feign.Param;
import feign.RequestLine;
import ru.ksenideni.touristmap.api.geocoding.model.GeocodingResponse;

public interface GeocodingClient {
    @RequestLine("GET /geocoding/v5/{endpoint}/{longitude},{latitude}.json?access_token={access_token}")
    GeocodingResponse getPlace(
            @Param("endpoint") String endpoint,
            @Param("longitude") double longitude,
            @Param("latitude") double latitude,
            @Param("access_token") String accessToken
    );
}
