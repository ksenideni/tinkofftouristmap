package ru.ksenideni.touristmap.api;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.ksenideni.touristmap.api.geocoding.GeocodingClient;
import ru.ksenideni.touristmap.api.geocoding.model.GeocodingResponse;

@Service
public class GeocodingApi {
    private final GeocodingClient geocodingClient;
    private final String token;
    private final String endpoint;

    @Autowired
    public GeocodingApi(
            @Value(value = "${api.geocoding.token}") String token,
            @Value(value = "${api.geocoding.endpoint}") String endpoint
    ) {
        this.geocodingClient = Feign
                .builder()
                .decoder(new JacksonDecoder())
                .target(GeocodingClient.class, "https://api.mapbox.com/");
        this.token = token;
        this.endpoint = endpoint;
    }

    public GeocodingResponse getPlace(double longitude, double latitude) {
        return geocodingClient.getPlace(endpoint, longitude, latitude, token);
    }
}
